# This is the base Dockerfile from which all other containers
# are built. It contains a basic rust installation with the stable
# toolchain, wasm-pack, and common cli tools.

# Note that this image can't build or check Amethyst, since it's
# missing dependencies to be more lightweight for building the book
# and linting/formatting. To build the engine, use the build-base Dockerfile.

FROM debian:latest

RUN apt-get update && \
  apt-get install -y \
  binutils-dev \
  build-essential \
  bash \
  cmake \
  curl \
  gcc \
  git \
  jq \
  wget \
  zip \
  unzip \
  ca-certificates \
  libasound2-dev \
  libcurl4-openssl-dev \
  libdw-dev \
  libelf-dev \
  libexpat1-dev \
  libfreetype6-dev \
  libiberty-dev \
  libsdl2-dev \
  libssl-dev \
  libx11-xcb-dev \
  libxcb1-dev \
  pkg-config \
  python2.7 \
  python3 \
  zlib1g-dev \
  nodejs \
  && \
  apt-get clean

COPY res/asound.conf /etc/asound.conf

RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash && \
  apt-get install git-lfs && \
  git lfs install

# We need to do this for the backtracking behaviour for the latest ${CHANNEL} with all available
# components, which may not be the current latest.
#
# See https://github.com/rust-lang/rustup/issues/2227
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --default-toolchain none
ENV PATH=/root/.cargo/bin:$PATH
RUN rustup toolchain install stable

RUN rustup component add rustfmt
RUN rustup component add clippy

RUN rustup target add wasm32-unknown-unknown
RUN curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh