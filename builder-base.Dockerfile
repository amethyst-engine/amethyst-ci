FROM {{CI_REGISTRY}}/amethyst-engine/amethyst-ci/base:latest

ARG KCOV_VERSION=36
ARG KCOV_BUILD_DIR=kcov-build

# --- kcov --- #
# For some reason `python` isn't the default executable file name.
# Needed for kcov
RUN ln -s /usr/bin/python2.7 /usr/bin/python

# Download kcov
# -nc is okay since we are downloading a tagged version
RUN wget -nc https://github.com/SimonKagstrom/kcov/archive/v$KCOV_VERSION.zip -O kcov.zip && \
  unzip -uoq kcov.zip && \
  rm kcov.zip

# Build KCov
RUN mkdir -p "kcov-$KCOV_VERSION/${KCOV_BUILD_DIR}" && \
  cd "kcov-$KCOV_VERSION/${KCOV_BUILD_DIR}" && \
  cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local -DCMAKE_BUILD_TYPE=Release && \
  make && \
  make install && \
  rm -rf "kcov-$KCOV_VERSION/${KCOV_BUILD_DIR}"