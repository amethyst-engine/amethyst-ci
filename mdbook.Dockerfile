FROM {{CI_REGISTRY}}/amethyst-engine/amethyst-ci/base:latest

ARG MDBOOK_VERSION=0.3.7

RUN cargo install mdbook --vers ${MDBOOK_VERSION}