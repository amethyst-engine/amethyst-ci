FROM {{CI_REGISTRY}}/amethyst-engine/amethyst-ci/builder-base:latest

RUN rustup toolchain install nightly

# Formatting and linting has already happened at this point,
# so we don't need to install any extra components for nightly.

RUN rustup default nightly